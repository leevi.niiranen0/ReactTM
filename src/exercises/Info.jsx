import React from "react";
import { Paper, Grid, Box } from "@material-ui/core/";
import EditBox from "./EditBox";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

const usesStyles = theme => ({
  //define style
  root: {
    padding: 2
  }
});

class Info extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showEdit: false };
  }

  onClick = () => {
    this.setState(prevState => ({ showEdit: !prevState.showEdit })); // toggle edit box
  };

  componentDidMount() {
    this.interval = setInterval(
      () => this.setState({ totime: this.props.totaltime() }),
      100
    ); //update total time each 0.1 of a second
  }

  componentWillUnmount() {
    clearInterval(this.interval); // clear update to avoid memmory leak?
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <Box className={classes.root}>
          {this.state.showEdit ? (
            <EditBox
              box={this.props.box}
              updateVal={this.props.updateVal}
              getTime={this.props.getTime}
              totaltime={this.props.totaltime}
            />
          ) : null}
          <Box width={300}>
            <Paper className={classes.root}>
              <Grid container>
                <Grid item xs={8}>
                  <p>{this.props.box.name}</p>
                </Grid>
                <Grid item xs={2}>
                  <IconButton onClick={this.onClick}>
                    <EditIcon />
                  </IconButton>
                </Grid>
                <Grid item xs={2}>
                  <IconButton
                    aria-label="delete"
                    onClick={() => this.props.delTask(this.props.box)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Grid>
              </Grid>
              <p>Task start: {this.props.box.timeStart}</p>
              <p>Task stop: {this.props.box.timeStop}</p>
              <p>
                Task duration:
                {this.props.getTime(
                  this.props.box.timeStart,
                  this.props.box.timeStop
                )}
              </p>
              <p>{this.props.box.desc}</p>
            </Paper>
          </Box>
        </Box>
      </React.Fragment>
    );
  }
}

export default withStyles(usesStyles, { withTheme: true })(Info);
