import React from "react";
import { Paper, Grid, Box } from "@material-ui/core/";
import TextField from "@material-ui/core/TextField";

import { withStyles } from "@material-ui/core/styles";

const usesStyles = theme => ({
  root: {
    padding: 2
  }
});

class EditBox extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Box width={440}>
        <Paper className={classes.root}>
          <Grid container>
            {/*  <Grid item xs={10}>
              <p>{this.props.box.name}</p>
            </Grid> */}
          </Grid>
          <form onChange={this.props.totaltime}>
            {/* update totaltime on change*/}
            <Grid container>
              <Grid item xs={6}>
                <TextField
                  id="name"
                  label="Task Name"
                  value={this.props.box.name}
                  onChange={this.props.updateVal.bind(this, this.props.box)}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="timeStart"
                  label="Start"
                  type="time"
                  value={this.props.box.timeStart}
                  onChange={this.props.updateVal.bind(this, this.props.box)}
                  InputLabelProps={{
                    shrink: true
                  }}
                  inputProps={{
                    step: 300 // 5 min
                  }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="timeStop"
                  label="Stop"
                  type="time"
                  value={this.props.box.timeStop}
                  onChange={this.props.updateVal.bind(this, this.props.box)}
                  InputLabelProps={{
                    shrink: true
                  }}
                  inputProps={{
                    step: 300 // 5 min
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  fullWidth
                  id="desc"
                  multiline
                  rows="4"
                  value={this.props.box.desc}
                  variant="outlined"
                  onChange={this.props.updateVal.bind(this, this.props.box)}
                />
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Box>
    );
  }
}

export default withStyles(usesStyles, { withTheme: true })(EditBox);
