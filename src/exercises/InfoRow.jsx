import React from "react";
import Grid from "@material-ui/core/Grid";
import Info from "./Info";
import Button from "@material-ui/core/Button";

class InfoRow extends React.Component {
  state = {
    totaltime: "0:00",
    boxes: [
      {
        //default example array
        name: "Task",
        timeStart: "14:00",
        timeStop: "14:30",
        desc: "Example Description"
      }
    ]
  };

  updateValue = (value, event) => {
    const boxes = [...this.state.boxes]; //copy boxes
    const index = boxes.indexOf(value); //get index of input
    boxes[index] = { ...value }; //update copied boxes with input
    const nam = event.target.id; //get form name
    boxes[index][nam] = event.target.value; //set new value to boxes array using name and value from form
    this.setState({ boxes }); //update real boxes
  };

  addNewbox = () => {
    const def = {
      name: "Task",
      timeStart: "14:00",
      timeStop: "14:30",
      desc: "Example Description"
    }; //default array
    const boxes = [...this.state.boxes, def]; //make a copy of array and add def
    this.setState({ boxes }); //update boxes
  };

  delTask = box => {
    const boxes = [...this.state.boxes];
    const index = boxes.indexOf(box);
    boxes.splice(index, 1); //splice 1 object from array using the index
    this.setState({ boxes });
    console.log(boxes);
  };

  parseTime = time => {
    time = time.split(":");
    var s = new Date(0, 0, 0, time[0], time[1], 0); //change time to a usable format
    return s;
  };

  getTime = (a, b) => {
    var s = this.parseTime(a);
    var e = this.parseTime(b);
    var dif = new Date(e.getTime() - s.getTime()); //get time difference by subtracting start and end times
    var h = dif.getHours();
    var m = dif.getMinutes();
    h = (h < 10 ? "0" : "") + h; //check if value is less than 10 if so add "0" before it. h=2 => 0+2 => 02
    m = (m < 10 ? "0" : "") + m;
    const diff = h + ":" + m;
    return diff;
  };

  getTotaltime = () => {
    var value = 0;
    var time = this.state.boxes;
    var x;
    for (x of time) {
      // for loop to get time differences from each task object and add them together as minutes
      var x1 = x.timeStart;
      var x2 = x.timeStop;
      x = this.getTime(x1, x2, "");
      x = x.split(":");
      var h = Math.floor(x[0] * 60); // convert hours to minutes
      var r = Math.floor(parseInt(h) + parseInt(x[1]));
      value = Math.floor(r + value);
    }
    h = value / 60; //calculate hours
    var rh = Math.floor(h);
    var m = Math.floor((h - rh) * 60); //calculate minutes
    m = (m < 10 ? "0" : "") + m; //check line 60
    const dif = rh + ":" + m;
    this.setState({ totaltime: dif });
  };

  render() {
    return (
      <div>
        <p>{this.state.totaltime} Total time</p>
        <p>{this.state.boxes.length} Tasks</p>
        <Button color="primary" variant="contained" onClick={this.addNewbox}>
          New Task
        </Button>
        <Grid container direction="row">
          {this.state.boxes.map((box, index) => (
            <Info
              key={index}
              box={box}
              updateVal={this.updateValue}
              delTask={this.delTask}
              getTime={this.getTime}
              totaltime={this.getTotaltime}
            />
          ))}
        </Grid>
      </div>
    );
  }
}

export default InfoRow;
