import React from "react";
import InfoRow from "../exercises/InfoRow";

export default {
  title: "Info",
  component: InfoRow
};

export const InfoBox = () => <InfoRow />;
