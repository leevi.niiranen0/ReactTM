React Time management Web Application Demo

- git clone https://gitlab.com/leevi.niiranen0/ReactTM.git
- cd ReactTM
- npm start

Bugs
- running on windows might add extra 2 hours to task duration

Todo
- shorten the "this.props..." functions on info.jsx and editbox.jsx
- padding, colors... aka better UX with material ui
